#include <iostream>
#include <time.h>
using namespace std;

struct Stats
{
	int ID, secondes;
};

void CreateList(Stats[], int);
void FormatOutPut(int);
void MinMaxAverage(Stats[], int);
void TimeAbove(Stats[], int);
void FullOutPut(Stats[], int);

int main()
{
	srand(time(NULL));
	int n = rand() % 25 + 25;
	Stats* List = new Stats[n];
	CreateList(List, n);
	FullOutPut(List, n);
	MinMaxAverage(List, n);
	TimeAbove(List, n);
	delete[] List;
	system("PAUSE");

}

void FormatOutPut(int value)
{
	char str[6] = {};
	int sec = value % 60;
	int min = value / 60;
	if (sec > 9)
	{
		str[3] = (char)sec / 10 + 48;
	}
	else
	{
		str[3] = '0';
	}
	str[2] = ':';
	str[4] = (char)sec % 10 + 48;
	str[0] = '0';
	str[1] = (char)min + 48;
	str[5] = '\0';
	cout << str;
}

void CreateList(Stats Data[], int _n)
{
	for (int i = 0; i < _n; i++)
	{
		Data[i].ID = i + 1;
		Data[i].secondes = rand() % 151 + 30;
	}
}

void MinMaxAverage(Stats ArrayStats[], int _n)
{
	int AverageTime = 0;
	int MinTime = ArrayStats[0].secondes;
	int MaxTime = ArrayStats[0].secondes;
	for (int i = 0; i < _n; i++)
	{
		if (MaxTime < ArrayStats[i].secondes)
		{

			MaxTime = ArrayStats[i].secondes;
		}
		else if (MinTime > ArrayStats[i].secondes)
		{
			MinTime = ArrayStats[i].secondes;
		}
		AverageTime += ArrayStats[i].secondes;
	}
	AverageTime /= _n;
	cout << "Max time : ";
	FormatOutPut(MaxTime);
	cout << "\nMin time : ";
	FormatOutPut(MinTime);
	cout << "\nAverage time : ";
	FormatOutPut(AverageTime);
	cout << endl;
}

void FullOutPut(Stats Data[], int _n)
{
	for (int i = 0; i < _n; i++)
	{
		cout << "ID" << Data[i].ID << ": ";
		FormatOutPut(Data[i].secondes);
		cout << endl;
	}
}

void TimeAbove(Stats ArrayStats[], int SizeArray)
{
	char Time[6];
	while (true)
	{
		cout << "Enter the time value in the format \"MM:SS\" " << endl;
		cin >> Time;
		if ((strlen(Time) == 5) && (Time[0] <= '9') && (Time[0] >= '0') && (Time[1] <= '9') && (Time[1] >= '0') && (Time[3] <= '9') && (Time[3] >= '0') && (Time[4] <= '9') && (Time[4] >= '0') && ((Time[2] == ':') || (Time[2] == '.') || (Time[2] == ',')))
		{
			break;
		}
		else
		{
			cout << "Incorrect Value" << endl;
		}
	}
	int EnteredTime = ((int(Time[0] - '0') * 10 + int(Time[1] - '0')) * 60 + int(Time[3] - '0') * 10 + int(Time[4] - '0'));
	for (int i = 0; i < SizeArray; i++)
	{
		if (EnteredTime < ArrayStats[i].secondes)
		{
			cout << "ID" << ArrayStats[i].ID << ": ";
			FormatOutPut(ArrayStats[i].secondes);
			cout << endl;
		}
	}
}